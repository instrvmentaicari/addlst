#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"

int main (int argc, char** argv){
 long int i= 0, t= 0; char c;
 while (read (0, &c, 1)){
  if (c=='\n'){i+= t, t= 0; continue;}
  t= t*10; t+= c&0xf;
 }
 printf ("%li\n", i); 
 return 0;
}
