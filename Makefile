addlst: addlst.c
	clang -O2 -mavx2 addlst.c -o addlst
install: addlst
	sudo cp ./addlst /usr/bin/
uninstall:
	sudo rm /usr/bin/addlst
clean:	addlst
	rm ./addlst
